export const Roles = Object.freeze({
    CUSTOMER: 'Customer',
    MANAGER: 'Manager',
    ADMINISTRATOR: 'Administrator',
    VISITOR: 'Visitor'
});